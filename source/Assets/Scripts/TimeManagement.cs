﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeManagement : MonoBehaviour
{
    public float dayTime = 20f;
    float time = 0f;

    // only the retrieve
    public Text dayText;
    public TouchButton getResource;
    public AudioClip newDay;
    public AudioClip treeGrowth;
    public Text loseDay;
    public static int day = 0;

    private void Update() {
        
    /*    time += Time.deltaTime;
        
        // if over 1 day
        if (time > dayTime) {

            SoundManagement.instance.PlaySoundOneShot(newDay);
            time -= dayTime;

            // update quest and tree
            QuestManagement.instance.UpdateQuest();

            Resource[] resources = FindObjectsOfType<Resource>();

            foreach (Resource resource in resources) {
                resource.UpdateResource();
            }

            getResource.UpdateText(3);


        }*/
    }

    private void OnTriggerEnter2D(Collider2D other) {

        Debug.Log(other.tag);
        if (other.tag != "Bound") return;

        day++;
        dayText.text = "Day: " + day.ToString();

        SoundManagement.instance.PlaySoundOneShot(newDay);
        time -= dayTime;

        // update quest and tree
        QuestManagement.instance.UpdateQuest();
        Resource[] resources = FindObjectsOfType<Resource>();

        if (resources.Length > 0) {
             SoundManagement.instance.PlaySoundOneShot(treeGrowth);
        }

        foreach (Resource resource in resources) {
            resource.UpdateResource();
        }

        getResource.UpdateText(3);


    }

}

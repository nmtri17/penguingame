using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class State: MonoBehaviour {
    
    public Sprite sprite;
    public int[] awards;

    State(Sprite sprite, int[] awards) {
        this.sprite = sprite;
        this.awards = awards;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class TouchObject : MonoBehaviour
{
    public GameObject subTree;
    public GameObject[] testTree;
    public Sprite[] firstSprites;
    public static int presentChoice = -3;
    public static GameObject presentObject;
    public Text text;

    public AudioClip collectSource;
    public AudioClip growSource;
    int score = 0;
    Color initCol;

    private void Awake() {
        initCol = GetComponent<SpriteRenderer>().color;
    }
    
    private void OnMouseOver() {
        
        //GetComponent<SpriteRenderer>().color = new Color(255,2,2,68);

        //if (EventSystem.current.IsPointerOverGameObject()) return;

        UIElementDragger.to = this;

        /*if (subTree != null) {
            int[] awards = subTree.GetComponent<Resource>().getCurrentAwards();
            UIManagement.instance.ShowDescription(awards);
        }
        else*/ 
        if (presentChoice >= 0) {
            // show preview
            GameObject testing = gameObject.transform.Find("Testing").gameObject;
            testing.GetComponent<SpriteRenderer>().sprite = firstSprites[presentChoice];
        }


        if (Input.GetMouseButtonDown(0)) {

            //MakeObj();
            if (subTree != null) {
                int[] awards = subTree.GetComponent<Resource>().getCurrentAwards();
                UIManagement.instance.ShowDescription(awards);
            }
            else {
                int[] awards = {0,0,0,0,0,0};
                UIManagement.instance.ShowDescription(awards);
            }

        }

    }

    public void MakeObj() {

        if (presentChoice == -1) {
                
            if (subTree == null) return;

                SoundManagement.instance.PlaySoundOneShot(collectSource);
                
                subTree.GetComponent<Resource>().CollectResource(transform);
                Destroy(subTree);

                presentObject.GetComponent<TouchButton>().UpdateText(-1);
                // add point
            }

            if (presentChoice >= 0) {

                // check if subtree is present
                if (subTree != null) return;
                if (presentObject.GetComponent<TouchButton>().amount <= 0) return;
                
                SoundManagement.instance.PlaySoundOneShot(growSource);

                // add game object
                GameObject newTree = Instantiate(testTree[presentChoice], transform.position, Quaternion.identity);
                newTree.transform.localScale *= 5;
                newTree.transform.parent = transform;
                subTree = newTree;

                // update present Object
                presentObject.GetComponent<TouchButton>().UpdateText(-1);
            }

    }

    private void OnMouseExit() {
        //GetComponent<SpriteRenderer>().color = initCol;

        GameObject testing = gameObject.transform.Find("Testing").gameObject;
        testing.GetComponent<SpriteRenderer>().sprite = null;

        if  (UIElementDragger.to == this) UIElementDragger.to = null;

    }
    

}

﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIElementDragger : EventTrigger {

    public static TouchObject to;
    private bool dragging;
    Vector3 initPos;

    private void Awake() {
    
        initPos = transform.localPosition;
    }


    public void Update() {
        if (dragging) {
            transform.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }
    }

    public override void OnPointerDown(PointerEventData eventData) {
        dragging = true;
        transform.parent.GetComponent<Button>().onClick.Invoke();
        
        MoveCamera.isLock = true;
    }

    public override void OnPointerUp(PointerEventData eventData) {
        dragging = false;
        transform.localPosition = initPos;
        //Debug.Log(GetComponent<RectTransform>().transform.position);

        if (to != null) to.MakeObj();
        

        MoveCamera.isLock = false;
    }
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ResourceManagement : MonoBehaviour
{
    public static ResourceManagement instance;
    public RectTransform[] destinations;
    public Sprite[] sprites;
    public GameObject collectedResource;

    private void Awake() {

        if (instance == null)
            instance = this;
        else
            Destroy(this);

    }

    public RectTransform findRectTransform(int index) {
        return destinations[index];
    }

    public Sprite findSprite(int index) {
        return sprites[index];
    }

    public void createIcon(Transform position, int index) {

        GameObject item = Instantiate(collectedResource, position.position, Quaternion.identity);
        item.GetComponent<CollectedItem>().SetSprite(sprites[index], destinations[index]);
        
    }






}
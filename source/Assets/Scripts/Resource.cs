﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Resource : MonoBehaviour
{
    // thing like seed, wood, ... to trade or to complete quest

    [Range(0,4)]
    public int Type;
    public State[] states;
    int period = 0;
    SpriteRenderer spriteRenderer;
    
    private void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = states[period].sprite;
    }
    
    public void CollectResource(Transform position) {
        
        int[] awards = getCurrentAwards();

        for (int i = 3; i < awards.Length; i++) {

            for (int j = 0; j < awards[i]; j++) {
                ResourceManagement.instance.createIcon(position, i - 3);
            }


        } 

        // random seeds
        UIManagement.instance.UpdateScore(getCurrentAwards());
        Destroy(gameObject);
    }

    public void UpdateResource() {

        period += 1;
        Debug.Log(states.Length);
        // check if resource die or not
        if (period >= states.Length) Destroy(gameObject);
        else
        // update sprite
            spriteRenderer.sprite = states[period].sprite;
    }

    public int[] getCurrentAwards() {
        return states[period].awards;
    }


}

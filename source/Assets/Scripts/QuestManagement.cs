using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
using UnityEngine.SceneManagement;

public class QuestManagement : MonoBehaviour
{
    public static QuestManagement instance;
    public Quest[] quests; 
    public Text[] requireText;
    public Text dayText;
    public AudioClip winClip;
    public Text lostDay;
    public GameObject canvas;
    int currentQuest = 0;
    public void Awake() {
        if (instance == null)
            instance = this;
        else
            Destroy(this);

        SetText();
    }

    public void NextQuest() {
        currentQuest++;
        if (currentQuest >= quests.Length) currentQuest = 0;
        SetText();
    }


    void SetText() {

        if (currentQuest >= quests.Length) return;

        for (int i = 3; i < quests[currentQuest].requirements.Length; i++) {
            int num = quests[currentQuest].requirements[i];
            requireText[i - 3].text =  num.ToString() + "X";
        }

        dayText.text =  quests[currentQuest].timeInterval.ToString() + " days left";
    }

    public void OnMouseDown() {
        
        //if (Input.GetMouseButtonDown(0)) {
            if (isComplete()) {
                
                SoundManagement.instance.PlaySoundOneShot(winClip);

                extractComplete();
                //Invoke("NextQuest", 2f);
                NextQuest();
            }
            
        //}
    }

    bool isComplete() {

        if (currentQuest >= quests.Length) return false;

        for (int i = 0; i < quests[currentQuest].requirements.Length; i++) {
            
            int num = UIManagement.instance.amountOfType[i];
            if (quests[currentQuest].requirements[i] > num) return false;
        }
        Debug.Log("Complete");
        return true;
    }

    void extractComplete() {

        for (int i = 0; i < quests[currentQuest].requirements.Length; i++) {
            
            int num = quests[currentQuest].requirements[i];
            UIManagement.instance.amountOfType[i] -= num;
        }

        int[] arr = {0, 0, 0, 0, 0, 0, 0, 0, 0};
        UIManagement.instance.UpdateScore(arr);
    }

    public void UpdateQuest() {

        if (currentQuest >= quests.Length) return;

        quests[currentQuest].timeInterval -= 1;
        if (quests[currentQuest].timeInterval == 0) {
            lostDay.text = TimeManagement.day.ToString();
            canvas.SetActive(true);
            Time.timeScale = 0;
        }
        SetText();
    }


    


}
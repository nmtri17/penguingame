﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MoveCamera : MonoBehaviour
{


    public static bool isLock = false;
    public float speed = 0.00000000000002f;
    public float startPoint = -8.8f;
    public float endPoint = 1.51f;
    Vector3 startVector, endVector;
    private void Awake() {
        startVector = new Vector3(startPoint, transform.position.y, transform.position.z);
        endVector = new Vector3(endPoint, transform.position.y, transform.position.z);
    }

    public float dragSpeed = 2;
    private Vector3 dragOrigin;

    void Update()
    {
        if (isLock) return;

       /* if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved) {
             Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
             transform.Translate( Mathf.Clamp(-touchDeltaPosition.x * speed, -0.8f, 0.8f), 0, 0);
        }*/

        if (Input.GetMouseButtonDown(0))
        {
            dragOrigin = Input.mousePosition;
            return;
        }
 
        if (!Input.GetMouseButton(0)) return;
 
        Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
        Vector3 move = new Vector3(pos.x * dragSpeed * Time.deltaTime, 0, 0);
 
        transform.Translate(-move);  


        if (transform.position.x > endPoint) transform.position = endVector;
        if (transform.position.x < startPoint) transform.position = startVector;
    }   
}

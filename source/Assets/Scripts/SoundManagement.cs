using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SoundManagement : MonoBehaviour {

    public static SoundManagement instance;
    AudioSource audioSource;

    private void Awake() {
        if (instance == null)
            instance = this;
        else
            Destroy(instance);

        audioSource = GetComponent<AudioSource>();
    }

    public void PlaySoundOneShot(AudioClip clip) {
        audioSource.PlayOneShot(clip);
    }



}
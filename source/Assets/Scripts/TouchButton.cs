﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchButton : MonoBehaviour
{
    [Range(-3,4)]
    public int type;
    public int amount;
    public Text amountText;
    public Sprite sprite;

    private void Awake() {

        if (type == -2) {
            //GetComponent<Button>(). = true;

        }


        
        if (amountText != null)
            amountText.text = amount.ToString();
        
        if (amountText != null && amount == 0) 
            GetComponent<Button>().interactable = false;
    }

    public void Add(GameObject typeButton) {

        TouchObject.presentChoice = type;
        TouchObject.presentObject = typeButton;

        CheckInteractable();
    }

    public void UpdateText(int number) {
        
        amount += number;
        if (type == -1) amount = Mathf.Clamp(amount,0,3);

        amountText.text = amount.ToString();

        //Debug.Log("Interable");
        CheckInteractable();
    }

    void CheckInteractable() {

        foreach (TouchButton button in FindObjectsOfType<TouchButton>()) {

            if (button.type == TouchObject.presentChoice || (button.type >= -1 && button.amount <= 0)) 
                button.GetComponent<Button>().interactable = false;
            else
                button.GetComponent<Button>().interactable = true;

        }


    }


}

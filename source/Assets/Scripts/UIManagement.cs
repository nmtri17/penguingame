using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManagement : MonoBehaviour
{
    public static UIManagement instance;
    [Tooltip("0 for seed, 1 for fruit, 2 for wood, 3 for flower")]
    public int[] amountOfType;
    public Text[] resourceAmount; 
    public Text[] description;

    public GameObject[] Materials;
    private void Awake() {
        if (instance == null)
            instance = this;
        else
            Destroy(instance);
    }

    public void UpdateScore(int[] awards) {

        for (int i = 0; i < amountOfType.Length; i++) {
            amountOfType[i] += awards[i];
        }

        string txt = "";
        for (int i = 0; i < amountOfType.Length; i++)
        {
            txt += "Type " + i.ToString() + " : " + amountOfType[i] + " ";
        }

        for (int i = 0; i < resourceAmount.Length; i++) {
            resourceAmount[i].text = amountOfType[i + 3].ToString() +  "X";
        }
    }

    public bool CheckResourse(int[] checking, int[] awards) {

        for (int i = 0; i < checking.Length; i++) {
            if (amountOfType[i] < checking[i]) return false;
            amountOfType[i] -= checking[i];
        }
        
        UpdateScore(awards);
        return true;
    }

    public void Award(int type, int amount) {
    
        Materials[type].GetComponent<TouchButton>().UpdateText(amount);
    }

    public void ShowDescription(int[] awards) {

       Debug.Log(awards.Length);
        for (int i = 0; i < description.Length; i++) {
            Debug.Log("Ok");
            description[i].text = awards[i + 3].ToString() +  "X";
        }

       // description.text = text;
    }
    



}
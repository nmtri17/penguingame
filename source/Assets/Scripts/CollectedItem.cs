﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectedItem : MonoBehaviour
{
    public RectTransform destination;
    public float speed = 2f;
    public float radius = 1f;
    //public AudioClip getSource;
    bool move = false;

    private void Update() {
        if (move) {
            Vector3 newDestination = Camera.main.ScreenToWorldPoint(destination.transform.position);
            transform.position = Vector3.Lerp(transform.position, newDestination, speed * Time.deltaTime);
            if (Vector3.Distance(transform.position, destination.position) < radius) {
                //SoundManagement.instance.PlaySoundOneShot(getSource);
                Destroy(gameObject);
            }
        }
    }

    public void SetSprite(Sprite sprite, RectTransform des) {
        GetComponent<SpriteRenderer>().sprite = sprite;
        destination = des;

        move = true;
    }


}

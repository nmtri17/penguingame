﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopBackground : MonoBehaviour
{
    public float speed = 2f;
    public float distance = 10f;
    public float loopX = -22;
    public float startX = 71.8f;

    private void Update() {
        
        transform.position = Vector3.Lerp(
            transform.position,
            transform.position + Vector3.left,
            speed * Time.deltaTime);

        if (transform.localPosition.x < loopX) {
            transform.localPosition = new Vector3(
                startX,
                transform.localPosition.y,
                transform.localPosition.z
            );
        }

        
    }




}
